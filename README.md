# OpenML dataset: Meta_Album_INS_2_Micro

https://www.openml.org/d/44248

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

## **Meta-Album Insects2 Dataset (Micro)**
***
The pest insects dataset was originally created as a large scale benchmark dataset for Insect Pest Recognition (https://github.com/xpwu95/IP102). It contains more than 75 000 images belongs to 102 categories. It also has a hierarchical taxonomy and the insect pests which mainly affect one specific agricultural product are grouped into the same upper-level category. The preprocessed version is made from the original dataset by cropping the images in perfect squares and then resizing them into the required images size of 128x128.  



### **Dataset Details**
![](https://meta-album.github.io/assets/img/samples/INS_2.png)

**Meta Album ID**: SM_AM.INS2  
**Meta Album URL**: [https://meta-album.github.io/datasets/INS_2.html](https://meta-album.github.io/datasets/INS_2.html)  
**Domain ID**: SM_AM  
**Domain Name**: Small Aninamls  
**Dataset ID**: INS_2  
**Dataset Name**: Insects2  
**Short Description**: Insects dataset for Insect Pest Recognition  
**\# Classes**: 20  
**\# Images**: 800  
**Keywords**: insects, ecology  
**Data Format**: images  
**Image size**: 128x128  

**License (original data release)**: Free for academic usage, cite to use dataset  
**License URL(original data release)**: https://github.com/xpwu95/IP102
 
**License (Meta-Album data release)**: CC BY-NC 4.0  
**License URL (Meta-Album data release)**: [https://creativecommons.org/licenses/by-nc/4.0/](https://creativecommons.org/licenses/by-nc/4.0/)  

**Source**: IP102: A Large-Scale Benchmark Dataset for Insect Pest Recognition  
**Source URL**: https://github.com/xpwu95/IP102  
  
**Original Author**: Xiaoping Wu, Chi Zhan, Yukun Lai, Ming-Ming Cheng, Jufeng Yang  
**Original contact**: xpwu95@163.com  

**Meta Album author**: Ihsan Ullah  
**Created Date**: 01 March 2022  
**Contact Name**: Ihsan Ullah  
**Contact Email**: meta-album@chalearn.org  
**Contact URL**: [https://meta-album.github.io/](https://meta-album.github.io/)  



### **Cite this dataset**
```
@inproceedings{Wu2019Insect,
  title={IP102: A Large-Scale Benchmark Dataset for Insect Pest Recognition},
  author={Xiaoping Wu and Chi Zhan and Yukun Lai and Ming-Ming Cheng and Jufeng Yang},
  booktitle={IEEE CVPR},
  pages={8787--8796},
  year={2019},
}
```


### **Cite Meta-Album**
```
@inproceedings{meta-album-2022,
        title={Meta-Album: Multi-domain Meta-Dataset for Few-Shot Image Classification},
        author={Ullah, Ihsan and Carrion, Dustin and Escalera, Sergio and Guyon, Isabelle M and Huisman, Mike and Mohr, Felix and van Rijn, Jan N and Sun, Haozhe and Vanschoren, Joaquin and Vu, Phan Anh},
        booktitle={Thirty-sixth Conference on Neural Information Processing Systems Datasets and Benchmarks Track},
        url = {https://meta-album.github.io/},
        year = {2022}
    }
```


### **More**
For more information on the Meta-Album dataset, please see the [[NeurIPS 2022 paper]](https://meta-album.github.io/paper/Meta-Album.pdf)  
For details on the dataset preprocessing, please see the [[supplementary materials]](https://openreview.net/attachment?id=70_Wx-dON3q&name=supplementary_material)  
Supporting code can be found on our [[GitHub repo]](https://github.com/ihsaan-ullah/meta-album)  
Meta-Album on Papers with Code [[Meta-Album]](https://paperswithcode.com/dataset/meta-album)  



### **Other versions of this dataset**
[[Mini]](https://www.openml.org/d/44292)  [[Extended]](https://www.openml.org/d/44326)

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/44248) of an [OpenML dataset](https://www.openml.org/d/44248). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/44248/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/44248/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/44248/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

